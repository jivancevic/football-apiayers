import requests
import json
import os
from datetime import datetime
from dateutil import parser
import pytz
from tzlocal import get_localzone


base_api = "http://localhost:5000/api/players"

print("This is a console application that fetches the data from the exposed API and saves it into a file")
    
local_tz = get_localzone()

# function that gets all players from api and writes added and updated players since last ingest into new log
def get_all_players():
    response = requests.get(base_api).json()

    # get list of all log files
    path = "./logs/all_players"
    files = sorted(os.listdir(path))

    # read ingest_timestamp from last log
    last_log_index = 0
    last_timestamp = datetime.min.replace(tzinfo=pytz.UTC)
    if len(files) > 0:
        last_log_index = int(files[len(files)-1].replace('players_', '').replace('.txt', '').lstrip('0'))
        with open(path + "/" + files[len(files)-1]) as last_log:
            last_timestamp = last_log.readline().rstrip("\n").replace("ingest_timestamp:", "")
            last_timestamp = datetime.strptime(last_timestamp, '%Y-%m-%d %H:%M:%S.%f').astimezone(local_tz)
    
    # check if player has been added or updated since last ingest, if true, add it to new log
    new_data = []
    for player in response:
        last_modified = player["last_modified"]
        d_time = parser.parse(last_modified).astimezone(local_tz)
        if d_time > last_timestamp:
            new_data.append(player)

    # create new log and write newly added and updated players
    new_log_path = path + "/" + "players_" + str(last_log_index+1).zfill(6) + ".txt"
    with open(new_log_path, "w") as new_log:
        new_log.write("ingest_timestamp:"+str(datetime.now())+"\n")
        json.dump(new_data, new_log)

# function that gets player by id from api and creates its log if it didnt exist or rewrites existing log if it has been updated since last ingest
def get_player_by_id(id):
    response = requests.get(base_api+"/"+str(id)).json()
    
    # check if player log already exists
    path = "./logs/player_by_id/player_" + str(id) + ".txt"
    update = False
    try:
        with open(path) as last_log:
            # check if player has been updated since last ingest
            last_timestamp = last_log.readline().rstrip("\n").replace("ingest_timestamp:", "")
            last_timestamp = datetime.strptime(last_timestamp, '%Y-%m-%d %H:%M:%S.%f').astimezone(local_tz)
            last_modified = response["last_modified"]
            d_time = parser.parse(last_modified).astimezone(local_tz)
            if d_time > last_timestamp:
                update = True
    except FileNotFoundError:
        update = True
    
    # if player log didnt exist - create log with player information
    # if player has been updated since last ingest - rewrite log with new player information
    if update:
        with open(path, "w") as new_log:
            new_log.write("ingest_timestamp:"+str(datetime.now())+"\n")
            json.dump(response, new_log)
    
# main function
def data_ingestion():
    print("Choose which operation you want to perform:\n",
          "\t1 - Get all players\n",
          "\t2 - Get player by id",)
    try:
        op = int(input())
    except ValueError:
        print("Please provide number.")
        data_ingestion()
    if op == 1:
        get_all_players()
    elif op == 2:
        while(True):
            try:
                id = int(input("Enter id of the player you want to fetch: "))
            except ValueError:
                print("Please provide integer.")
                continue
            get_player_by_id(id)
            break
    else:
        print("Invalid command.")
        data_ingestion()

data_ingestion()
