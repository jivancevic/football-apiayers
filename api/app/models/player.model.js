module.exports = (sequelize, Sequelize) => {
    const player = sequelize.define('Player', {
        // attributes
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER    
        },
        first_name: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        last_name: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        current_club: {
            allowNull: true,
            type: Sequelize.STRING,
        },
        nationality: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        birth_date: {
            allowNull: false,
            type: Sequelize.DATEONLY,
        },
        preferred_position: {
            allowNull: true,
            type: Sequelize.STRING,
        },
        last_modified: {
            allowNull: false,
            type: Sequelize.DATE,
        },
    });

    return player;
};