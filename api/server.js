const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const playersRouter = require("./app/routes/players.routes")

const app = express();

//const corsOptions = {
//  origin: "http://localhost:5000"
//};

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
db.sequelize.sync();

app.use('/api/players', playersRouter);

// set port, listen for requests
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});