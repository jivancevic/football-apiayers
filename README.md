# Football APIayers

### A simple API that allows all CRUD actions on football players, and a simple console application/script that fetches the data from the API and stores it into a file.

Developed as a job task for C&I Ltd.

## Utilises:
- Node.js - back-end environment
- Express - handling HTTP requests
- Sequelize - ORM 
- Postgres - database
- Python - console application

## Overview of the Rest APIs:

- POST    `api/players`             add new player
- GET     `api/players`	            get all players
- GET     `api/players/:id`         get player by id
- PUT     `api/players/:id`         update player by id
- DELETE  `api/players/:id`         remove player by id
- DELETE  `api/players`             remove all players

## How to start REST server

- clone this project repository
- create a `football-apiayers` database in PostgreSQL
- in `api/app/db/db.config.js` replace the `HOST`, `USER` and `PASSWORD` field according to your info
- navigate to the repo folder in your terminal
- run command `cd api`
- run command `npm install` to install node dependencies
- run command `npm start` to start server

## How to import Postman collection

- open Postman application
- click on the file tab and then click import
- click on Upload Files
- choose `CRUD Football APIayers.postman_collection.json` and press Open

## How to run console application

- make sure REST server is running
- navigate to the repo folder in your terminal
- run command `cd api-ingestion`
- run command `pip install -r requirements.txt` to install required python packages
- run command `python3 apiayers-ingestion.py` to start application
